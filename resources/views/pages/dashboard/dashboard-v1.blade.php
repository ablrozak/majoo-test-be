@extends('layout.app')

@section('title','Dashboard - V1')

@section('content')
<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0">Dashboard</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                    <li class="breadcrumb-item active">Dashboard v1</li>
                </ol>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <!-- /.col (LEFT) -->
            <div class="col-md-12">
                <!-- LINE CHART -->
                <div class="card card-info">
                    <div class="card-header">
                        <h3 class="card-title">Line Chart</h3>

                        <div class="card-tools">
                            <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                <i class="fas fa-minus"></i>
                            </button>
                            <button type="button" class="btn btn-tool" data-card-widget="remove">
                                <i class="fas fa-times"></i>
                            </button>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="chart">
                            <canvas id="lineChart"
                                style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>
                        </div>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->

                <!-- BAR CHART -->
                <div class="card card-success">
                    <div class="card-header">
                        <h3 class="card-title">Combine Chart</h3>

                        <div class="card-tools">
                            <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                <i class="fas fa-minus"></i>
                            </button>
                            <button type="button" class="btn btn-tool" data-card-widget="remove">
                                <i class="fas fa-times"></i>
                            </button>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="chart">
                            <canvas id="combineChart"
                                style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>
                        </div>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->

                <!-- STACKED BAR CHART -->
                <div class="card card-success">
                    <div class="card-header">
                        <h3 class="card-title">Stacked Bar Chart</h3>

                        <div class="card-tools">
                            <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                <i class="fas fa-minus"></i>
                            </button>
                            <button type="button" class="btn btn-tool" data-card-widget="remove">
                                <i class="fas fa-times"></i>
                            </button>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="chart">
                            <canvas id="stackedBarChart"
                                style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>
                        </div>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->

            </div>
            <!-- /.col (RIGHT) -->
        </div>
        <!-- /.row -->
    </div><!-- /.container-fluid -->
</section>
{{-- <section class="content">
    <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <div class="row">
            <div class="col-lg-3 col-6">
                <!-- small box -->
                <div class="small-box bg-info">
                    <div class="inner">
                        <h3>150</h3>

                        <p>New Orders</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-bag"></i>
                    </div>
                    <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-3 col-6">
                <!-- small box -->
                <div class="small-box bg-success">
                    <div class="inner">
                        <h3>53<sup style="font-size: 20px">%</sup></h3>

                        <p>Bounce Rate</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-stats-bars"></i>
                    </div>
                    <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-3 col-6">
                <!-- small box -->
                <div class="small-box bg-warning">
                    <div class="inner">
                        <h3>44</h3>

                        <p>User Registrations</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-person-add"></i>
                    </div>
                    <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-3 col-6">
                <!-- small box -->
                <div class="small-box bg-danger">
                    <div class="inner">
                        <h3>65</h3>

                        <p>Unique Visitors</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-pie-graph"></i>
                    </div>
                    <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <!-- ./col -->
        </div>
        <!-- /.row -->
        <!-- Main row -->
        <div class="row">
            <!-- right col (We are only adding the ID to make the widgets sortable)-->
            <section class="col-lg-12 connectedSortable">
                <!-- solid sales graph -->
                <div class="card bg-gradient-info">
                    <div class="card-header border-0">
                        <h3 class="card-title">
                            <i class="fas fa-th mr-1"></i>
                            Sales Graph
                        </h3>

                        <div class="card-tools">
                            <button type="button" class="btn bg-info btn-sm" data-card-widget="collapse">
                                <i class="fas fa-minus"></i>
                            </button>
                            <button type="button" class="btn bg-info btn-sm" data-card-widget="remove">
                                <i class="fas fa-times"></i>
                            </button>
                        </div>
                    </div>
                    <div class="card-body">
                        <canvas class="chart" id="line-chart"
                            style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>
                    </div>
                    <!-- /.card-body -->
                    <div class="card-footer bg-transparent">
                        <div class="row">
                            <div class="col-4 text-center">
                                <input type="text" class="knob" data-readonly="true" value="20" data-width="60"
                                    data-height="60" data-fgColor="#39CCCC">

                                <div class="text-white">Mail-Orders</div>
                            </div>
                            <!-- ./col -->
                            <div class="col-4 text-center">
                                <input type="text" class="knob" data-readonly="true" value="50" data-width="60"
                                    data-height="60" data-fgColor="#39CCCC">

                                <div class="text-white">Online</div>
                            </div>
                            <!-- ./col -->
                            <div class="col-4 text-center">
                                <input type="text" class="knob" data-readonly="true" value="30" data-width="60"
                                    data-height="60" data-fgColor="#39CCCC">

                                <div class="text-white">In-Store</div>
                            </div>
                            <!-- ./col -->
                        </div>
                        <!-- /.row -->
                    </div>
                    <!-- /.card-footer -->
                </div>
                <!-- /.card -->
            </section>
            <!-- right col -->
        </div>
        <!-- /.row (main row) -->
    </div><!-- /.container-fluid -->
</section> --}}
<!-- /.content -->
@stop
@section('scripts')
<script>
    $(function () {
        var lineChartCanvas = $('#lineChart').get(0).getContext('2d')
        var combineChartCanvas = $('#combineChart').get(0).getContext('2d')
        var stackedBarChartCanvas = $('#stackedBarChart').get(0).getContext('2d')
        var labels1 = <?php echo json_encode($label_chart1, JSON_HEX_TAG); ?>;
        var dataset1 = <?php echo json_encode($dataset_chart1, JSON_HEX_TAG); ?>;
        
        var lineChart = new Chart(lineChartCanvas, {
            "responsive": false,
            "type":"line",
            "data":{
                "labels":labels1,
                "datasets":dataset1
            },
            plugins: [ChartDataLabels],
            options: {
                plugins: {
                    datalabels: {
                        labels: {
                            title: {
                                font: {
                                    weight: 'bold'
                                },
                                anchor:"end"
                            }
                        }
                    }
                },
                responsive: true,
                legend: {
                    position: "bottom"
                },
                title: {
                    display: true,
                    text: "Availability This Period"
                }
            }
        })

        var combineChart = new Chart(combineChartCanvas, {
            type: 'bar',
            data: {
                datasets: [{
                    label: 'Bar Dataset',
                    data: [15, 20, 30, 40],
                    backgroundColor: 'rgb(31, 104, 240)',
                    borderColor: 'rgb(31, 104, 240)',
                    borderWidth: 2,
                    datalabels: {
                        color: 'rgb(38, 38, 38)',
                        labels: {
                            name: {
                                // textAlign: 'bottom',
                                anchor:'end',
                                font: {
                                    size: 16,
                                    weight:'bold'
                                },
                            }
                        }
                    }
                }, {
                    label: 'Line1 Dataset',
                    data: [43, 65, 70, 80],
                    fill:false,
                    type: 'line',
                    borderWidth: 2,
                    borderColor: 'rgb(255, 99, 132)',
                    backgroundColor: 'rgb(255, 99, 132)',
                    pointBackgroundColor: 'rgb(255, 99, 132)',
                    pointStyle:'round',
                    datalabels: {
                        labels: {
                            name: {
                                anchor:'end',
                                font: {
                                    size: 16,
                                    weight:'bold'
                                },
                            }
                        }
                    }
                }, {
                    label: 'Line2 Dataset',
                    data: [50, 60, 88, 99],
                    fill:false,
                    type: 'line',
                    borderWidth: 2,
                    borderColor: 'rgb(118, 232, 77)',
                    backgroundColor: 'rgb(118, 232, 77)',
                    pointBackgroundColor: 'rgb(118, 232, 77)',
                    pointStyle:'round',
                    datalabels: {
                        labels: {
                            name: {
                                // align: 'bottom',
                                anchor:'end',
                                font: {
                                    size: 16,
                                    weight:'bold'
                                },
                            }
                        }
                    }
                }],
                labels: ['January', 'February', 'March', 'April']
            },
            plugins: [ChartDataLabels],
            options: {
                responsive: true,
                legend: {
                    position: "bottom"
                },
                title: {
                    display: true,
                    text: "Availability This Period"
                }
            }
        })
        
        var stackedBarChart = new Chart(stackedBarChartCanvas, {
            type: 'line',
            data: {
                datasets: [{
                    label: 'Line1 Dataset',
                    data: [175, 259, 297, 341],
                    fill:true,
                    borderWidth: 2,
                    borderColor: 'rgb(55, 88, 237)',
                    backgroundColor: 'rgb(55, 88, 237)',
                    pointBackgroundColor: 'rgb(55, 88, 237)',
                    pointStyle:'round',
                    datalabels: {
                        labels: {
                            name: {
                                anchor:'center',
                                font: {
                                    size: 16,
                                    weight:'bold'
                                },
                            }
                        }
                    }
                },
                {
                    label: 'Line1 Dataset',
                    data: [203, 295, 385, 460],
                    fill:true,
                    borderWidth: 2,
                    borderColor: 'rgb(219, 104, 33)',
                    backgroundColor: 'rgb(219, 104, 33)',
                    pointBackgroundColor: 'rgb(219, 104, 33)',
                    pointStyle:'round',
                    datalabels: {
                        labels: {
                            name: {
                                anchor:'center',
                                font: {
                                    size: 16,
                                    weight:'bold'
                                },
                            }
                        }
                    }
                },
                {
                    label: 'Line1 Dataset',
                    data: [206, 306, 421, 474],
                    fill:true,
                    borderWidth: 2,
                    borderColor: 'rgb(143, 133, 127)',
                    backgroundColor: 'rgb(143, 133, 127)',
                    pointBackgroundColor: 'rgb(143, 133, 127)',
                    pointStyle:'round',
                    datalabels: {
                        labels: {
                            name: {
                                anchor:'center',
                                font: {
                                    size: 16,
                                    weight:'bold'
                                },
                            }
                        }
                    }
                }],
                labels: ['January', 'February', 'March', 'April']
            },
            plugins: [ChartDataLabels],
            options: {
                responsive: true,
                legend: {
                    position: "bottom"
                },
                title: {
                    display: true,
                    text: "Availability This Period"
                }
            }
        })
    })
</script>
@endsection