@extends('layout.app')

@section('title','Dashboard - V2')

@section('content')
<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0">Dashboard</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                    <li class="breadcrumb-item active">Dashboard - V2</li>
                </ol>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <form action="/dashboard-v2-filter" method="POST">
            @csrf
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Year:</label>
                                <div class="input-group mb-3">
                                    <select class="custom-select" name="year">
                                        @for($i = 0; $i < 3; $i++) 
                                        @php $str='-' .$i.' years';
                                            $year=date('Y',strtotime($str)); 
                                        @endphp 
                                        <option value={{$year}}>{{$year}}</option>
                                    @endfor
                                    </select>
                                    <div class="input-group-append">
                                        <button type="submit" class="btn btn-md btn-default">
                                            <i class="fa fa-search"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group">
                                <label>Region:</label>
                                <select class="custom-select" name="region">
                                    <option value='all_region'>All Region</option>
                                    @foreach ($regions as $region)
                                    <option value={{$region}}>{{$region}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </form>
        <div class="row">
            <!-- /.col (LEFT) -->
            <div class="col-md-12">
                <!-- STACKED BAR CHART -->
                <div class="card card-success">
                    <div class="card-header">
                        <h3 class="card-title">Stacked Bar Chart</h3>

                        <div class="card-tools">
                            <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                <i class="fas fa-minus"></i>
                            </button>
                            <button type="button" class="btn btn-tool" data-card-widget="remove">
                                <i class="fas fa-times"></i>
                            </button>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="chart">
                            <canvas id="stackedBarChart"
                                style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>
                        </div>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->

            </div>
            <!-- /.col (RIGHT) -->
        </div>
        <!-- /.row -->
    </div><!-- /.container-fluid -->
</section>
@stop
@section('scripts')
<script>
    $(function () {
        $('.select2').select2()
        var stackedBarChartCanvas = $('#stackedBarChart').get(0).getContext('2d')
        var label = <?php echo json_encode($label, JSON_HEX_TAG); ?>;
        var dataset = <?php echo json_encode($dataset, JSON_HEX_TAG); ?>;
        var title = <?php echo json_encode($title, JSON_HEX_TAG); ?>;
        var stackedBarChart = new Chart(stackedBarChartCanvas, {
            type: 'bar',
            data: {
                datasets: dataset,
                labels: label
            },
            // plugins: [ChartDataLabels],
            options: {
                responsive: true,
                legend: {
                    position: "bottom"
                },
                title: {
                    display: true,
                    text: title
                },
                "animation": {
                    "duration": 0,
                    "onComplete": function() {
                        var chartInstance = this.chart,
                        ctx = chartInstance.ctx;
        
                        ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, Chart.defaults.global.defaultFontStyle, Chart.defaults.global.defaultFontFamily);
                        ctx.textAlign = 'center';
                        ctx.textBaseline = 'up';
        
                        this.data.datasets.forEach(function(dataset, i) {
                            var meta = chartInstance.controller.getDatasetMeta(i);
                            meta.data.forEach(function(bar, index) {
                                var data = dataset.data[index];
                                ctx.fillText(data, bar._model.x, bar._model.y - 5);
                            });
                        });
                    }
                },
                hover: {
                    animationDuration: 0, // duration of animations when hovering an item
                },
                responsiveAnimationDuration: 0, // animation duration after a resize
                scales: {
                    yAxes: [{
                        ticks: {
                            fontSize:15
                        }
                    }],
                    xAxes: [{
                        ticks: {
                            fontSize:16,
                        }
                    }]
                }
            }
        })
    })
</script>
@endsection