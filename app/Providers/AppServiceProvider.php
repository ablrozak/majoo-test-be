<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Response;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Response::macro('success', function ($data) {
            return Response::json([
              'success'  => true,
              'data' => $data,
            ]);
        });
    
        Response::macro('error', function ($message, $status = 400) {
            return Response::json([
              'success'  => false,
              'message' => $message,
            ], $status);
        });
    }
}
