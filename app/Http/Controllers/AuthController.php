<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Validator;

class AuthController extends Controller
{
    /**
     * Get a JWT via given credentials.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'user_name' => 'required',
            'password' => 'required|string|min:6',
        ]);

        if ($validator->fails()) {
            return response()->error($validator->errors());
        }

        $user = User::where('user_name', $request->user_name)
            ->where('password', md5($request->password))
            ->first();
        if (!$user) {
            return response()->error('Invalid credential');
        }
        if (!$token = Auth::login($user)) {
            return response()->error('Unauthorized');
        }

        return $this->createNewToken($token);
    }

    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function createNewToken($token)
    {
        $value = array(
            'success' => true,
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth()->factory()->getTTL() * 60,
            'user' => auth()->user(),
            'url' => route('home')
        );
        session()->put('auth.isauthenticated', time() . '_' . md5(time()));
        session()->put('auth.access_token', $value['access_token']);
        session()->put('auth.token_type', $value['token_type']);
        session()->put('auth.user_name', $value['user']['user_name']);
        session()->put('auth.name', $value['user']['name']);
        return response()->json($value);
    }

}
