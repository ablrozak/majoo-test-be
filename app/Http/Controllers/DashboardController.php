<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\MigrationUpdate;
use DB;

class DashboardController extends Controller
{
    public function dashboardv1()
    {
        $labels1 = array();
        $dataset1 = array();
        for ($i = 1; $i <= 31; $i++) {
            array_push($labels1, $i);
        }
        for ($i = 0; $i < 2; $i++) {
            if ($i == 0) {
                $label = 'Acc Plan Oct-21';
            } else {
                $label = 'Acc Ach Oct-21';
            }
            $dt = array();
            for ($x = 1; $x <= 31; $x++) {
                array_push($dt, rand(10, 100));
            }
            $color = sprintf('#%06X', mt_rand(0, 0xFFFFFF));
            $dataset1[$i]['label'] = $label;
            $dataset1[$i]['backgroundColor'] = $color;
            $dataset1[$i]['borderColor'] = $color;
            $dataset1[$i]['borderWidth'] = 1;
            $dataset1[$i]['fill'] = false;
            $dataset1[$i]['data'] = $dt;
        }
        $data['label_chart1'] = $labels1;
        $data['dataset_chart1'] = $dataset1;
        return view('pages.dashboard.dashboard-v1',$data);
    }

    public function dashboardv2(Request $request)
    {
        $where="1=1";
        $region_selected = 'all';
        if($request->has('region')){
            $region_selected = $request->region;
            $where = $where."region = '".$request->region."'";
        }
        $year = ($request->has('year'))?$request->year:date('Y');
        $regions = MigrationUpdate::whereraw(DB::raw("YEAR(periode)='".$year."'"))->select(DB::raw("distinct vendor"))->pluck('vendor');
        $label_yearly = array();
        foreach($regions as $x => $region){
            $mig_up_datas = MigrationUpdate::whereraw(DB::raw("YEAR(periode)='".$year."' AND vendor ='".$region."'"))->orderBy('periode','asc')->get();
            foreach($mig_up_datas as $mig_up_data){
                $total[date('M-y',strtotime($mig_up_data->periode))]['total'] = $mig_up_data->total;
            }
            for ($i = 0; $i <= 11; $i++) {
                $conv_periode = date('M-y', strtotime('01-' . sprintf("%02d", $i + 1) . '-' . $year));
                if ($x == 0) {
                    array_push($label_yearly, $conv_periode);
                }
                // return $total[0];
                if (array_key_exists($conv_periode, $total)) {
                    $value[$i] = $total[$conv_periode]['total'];
                } else {
                    $value[$i] = 0;
                }
                
            }
            $color = sprintf('#%06X', mt_rand(0, 0xFFFFFF));
            $dataset[$x]['label']=$region;
            $dataset[$x]['backgroundColor']=$color;
            $dataset[$x]['borderColor']=$color;
            $dataset[$x]['borderWidth']=1;
            $dataset[$x]['data']=$value;
        }
        $data['label'] = $label_yearly;
        $data['dataset'] = $dataset;
        $data['title'] = 'Migration Update '.$year;
        $data['regions'] = $regions;
        $data['year_selected'] = $year;
        $data['region_selected'] = $region_selected;
        return view('pages.dashboard.dashboard-v2',$data);
    }

    public function dashboardv2Filter(Request $request)
    {
        $where="1=1";
        $region_selected = 'All Region';
        if($request->has('region') && $request->region != 'all_region'){
            $region_selected = $request->region;
            $where = $where." AND region = '".$request->region."'";
        }
        $year = ($request->has('year'))?$request->year:date('Y');
        $all_regions = MigrationUpdate::select(DB::raw("distinct region"))->pluck('region');
        $regions = MigrationUpdate::whereraw(DB::raw($where))->select(DB::raw("distinct region"))->pluck('region');
        $label_yearly = array();
        foreach($regions as $x => $region){
            $mig_up_datas = MigrationUpdate::where('region',$region)->orderBy('periode','asc')->get();
            foreach($mig_up_datas as $mig_up_data){
                $total[date('M-y',strtotime($mig_up_data->periode))]['total'] = $mig_up_data->total;
            }
            for ($i = 0; $i <= 11; $i++) {
                $conv_periode = date('M-y', strtotime('01-' . sprintf("%02d", $i + 1) . '-' . $year));
                if ($x == 0) {
                    array_push($label_yearly, $conv_periode);
                }
                // return $total[0];
                if (array_key_exists($conv_periode, $total)) {
                    $value[$i] = $total[$conv_periode]['total'];
                } else {
                    $value[$i] = 0;
                }
                
            }
            $color = sprintf('#%06X', mt_rand(0, 0xFFFFFF));
            $dataset[$x]['label']=$region;
            $dataset[$x]['backgroundColor']=$color;
            $dataset[$x]['borderColor']=$color;
            $dataset[$x]['borderWidth']=1;
            $dataset[$x]['data']=$value;
        }
        $data['label'] = $label_yearly;
        $data['dataset'] = $dataset;
        $data['title'] = 'Migration Update '.$year.' '.$region_selected;
        $data['regions'] = $all_regions;
        $data['year_selected'] = $year;
        $data['region_selected'] = $region_selected;
        return view('pages.dashboard.dashboard-v2',$data);
    }
}
