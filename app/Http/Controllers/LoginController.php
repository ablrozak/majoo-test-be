<?php

namespace App\Http\Controllers;

use App\Http\Controllers\AuthController;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    public function index()
    {
        return view('auth.login');
    }

    public function attempt(Request $request)
    {
        $auth = new AuthController();
        $res = $auth->login($request);
        return response()->json($res);
    }

    public function logout()
    {
        session()->forget('auth');
        return redirect()->route('home');
    }
}
