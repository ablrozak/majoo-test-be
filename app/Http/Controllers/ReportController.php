<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Transaction;
use App\Models\Merchant;
use App\Models\Outlet;
use JWTAuth;
use Validator;
use DB;

class ReportController extends Controller
{
    public function reportMerchants(Request $request)
    {
        try {
            $limit = ($request->has('limit'))?$request->limit:10;
            $token= request()->bearerToken();
            $user = JWTAuth::setToken($token)->toUser();
            $merchant = Merchant::where('user_id',$user->id)->first();
            if(!$merchant){
                return response()->json(['message'=>'Merchant not found'], 404);
            }
            $query = DB::table(DB::raw("Transactions t
                            left join Merchants m on t.merchant_id =m.id"))
                        ->where('t.merchant_id',$merchant->id)
                        ->select(DB::raw("m.merchant_name ,sum(t.bill_total) omzet, cast(t.created_at as date) trx_date"))
                        ->groupBy(DB::raw("t.merchant_id ,m.merchant_name ,cast(t.created_at as date)"));
            $data = DB::table('cte_date')
                        ->select(DB::raw("IFNULL(cte_trx.merchant_name,'".$merchant->merchant_name."') merchant_name,IFNULL(cte_trx.omzet,0) omzet ,cte_date.Date as trx_date"))
                        ->withExpression('cte_date', "select a.Date 
                            from (
                                select curdate() - INTERVAL (a.a + (10 * b.a) + (100 * c.a) + (1000 * d.a) ) DAY as Date
                                from (select 0 as a union all select 1 union all select 2 union all select 3 union all select 4 union all select 5 union all select 6 union all select 7 union all select 8 union all select 9) as a
                                cross join (select 0 as a union all select 1 union all select 2 union all select 3 union all select 4 union all select 5 union all select 6 union all select 7 union all select 8 union all select 9) as b
                                cross join (select 0 as a union all select 1 union all select 2 union all select 3 union all select 4 union all select 5 union all select 6 union all select 7 union all select 8 union all select 9) as c
                                cross join (select 0 as a union all select 1 union all select 2 union all select 3 union all select 4 union all select 5 union all select 6 union all select 7 union all select 8 union all select 9) as d
                            ) a
                            where a.Date between '2021-11-01' and '2021-11-30'
                            order by a.Date")
                        ->withExpression('cte_trx', $query)
                        ->leftJoin('cte_trx', 'cte_trx.trx_date', '=', 'cte_date.Date')
                        ->orderBy('cte_date.Date','asc')
                        ->paginate($limit);
            return response()->success($data);
        } catch (\Exception $e) {
            $msg['file'] = $e->getFile();
            $msg['message'] = $e->getMessage();
            $msg['line'] = $e->getLine();
            \Log::error($msg);
            return response()->error('Internal Server Error');
        } catch (\Illuminate\Database\QueryException $e) {
            $msg['file'] = $e->getFile();
            $msg['message'] = $e->getMessage();
            $msg['line'] = $e->getLine();
            \Log::error($msg);
            return response()->error('Database Error');
        }
    }

    public function reportOutlets(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'outlet_id' => 'required',
            ]);
            if ($validator->fails()) {
                return response()->error($validator->errors());
            }
            $limit = ($request->has('limit'))?$request->limit:10;
            $token= request()->bearerToken();
            $user = JWTAuth::setToken($token)->toUser();
            $outlet = Outlet::leftJoin('Merchants', 'Outlets.merchant_id', '=', 'Merchants.id')
                        ->leftJoin('Users','Merchants.user_id','=','Users.id')
                        ->where('Outlets.id',$request->outlet_id)
                        ->where('Users.id',$user->id)
                        ->select(DB::raw("Outlets.id as outlet_id,Outlets.outlet_name,Merchants.merchant_name,Merchants.id as merchant_id"))
                        ->first();
            if(!$outlet){
                return response()->error('Outlet not found');
            }
            $query = DB::table(DB::raw("Transactions t
                            left join Merchants m on t.merchant_id =m.id
                            left join Outlets o on t.outlet_id =o.id"))
                        ->where('t.merchant_id',$outlet->merchant_id)
                        ->where('t.outlet_id',$outlet->outlet_id)
                        ->select(DB::raw("m.merchant_name,o.outlet_name,sum(t.bill_total) omzet, cast(t.created_at as date) trx_date"))
                        ->groupBy(DB::raw("t.merchant_id,o.outlet_name,m.merchant_name ,cast(t.created_at as date)"));
            $data = DB::table('cte_date')
                        ->select(DB::raw("IFNULL(cte_trx.merchant_name,'".$outlet->merchant_name."') merchant_name
                            ,IFNULL(cte_trx.outlet_name,'".$outlet->outlet_name."') outlet_name
                            ,IFNULL(cte_trx.omzet,0) omzet ,cte_date.Date as trx_date"))
                        ->withExpression('cte_date', "select a.Date 
                            from (
                                select curdate() - INTERVAL (a.a + (10 * b.a) + (100 * c.a) + (1000 * d.a) ) DAY as Date
                                from (select 0 as a union all select 1 union all select 2 union all select 3 union all select 4 union all select 5 union all select 6 union all select 7 union all select 8 union all select 9) as a
                                cross join (select 0 as a union all select 1 union all select 2 union all select 3 union all select 4 union all select 5 union all select 6 union all select 7 union all select 8 union all select 9) as b
                                cross join (select 0 as a union all select 1 union all select 2 union all select 3 union all select 4 union all select 5 union all select 6 union all select 7 union all select 8 union all select 9) as c
                                cross join (select 0 as a union all select 1 union all select 2 union all select 3 union all select 4 union all select 5 union all select 6 union all select 7 union all select 8 union all select 9) as d
                            ) a
                            where a.Date between '2021-11-01' and '2021-11-30'
                            order by a.Date")
                        ->withExpression('cte_trx', $query)
                        ->leftJoin('cte_trx', 'cte_trx.trx_date', '=', 'cte_date.Date')
                        ->orderBy('cte_date.Date','asc')
                        ->paginate($limit);
            return response()->success($data);
        } catch (\Exception $e) {
            $msg['file'] = $e->getFile();
            $msg['message'] = $e->getMessage();
            $msg['line'] = $e->getLine();
            \Log::error($msg);
            return response()->error('Internal Server Error');
        } catch (\Illuminate\Database\QueryException $e) {
            $msg['file'] = $e->getFile();
            $msg['message'] = $e->getMessage();
            $msg['line'] = $e->getLine();
            \Log::error($msg);
            return response()->error('Database Error');
        }
    }

    public function bilDeret(Request $request)
    {
        try {
            $num1 = $request->num1;
            $num2 = $request->num2;
            $num3 = $request->num3;
            $interval = $num2-$num1;
            $newnum = $num1;
            $string = "";
            for($i=1;$i<=$num3;$i++){
                $string = $string." ".$newnum;
                $newnum = $newnum+$interval;
            }
            return response()->success($string);
        } catch (\Exception $e) {
            $msg['file'] = $e->getFile();
            $msg['message'] = $e->getMessage();
            $msg['line'] = $e->getLine();
            \Log::error($msg);
            return response()->error('Internal Server Error');
        } catch (\Illuminate\Database\QueryException $e) {
            $msg['file'] = $e->getFile();
            $msg['message'] = $e->getMessage();
            $msg['line'] = $e->getLine();
            \Log::error($msg);
            return response()->error('Database Error');
        }
    }

    public function sorting(Request $request)
    {
        try {
            $array = array(4, -7, -5, 3, 3.3, 9, 0, 10, 0.2);
            for($i=1;$i<count($array);$i++){
                for($j=0;$j<count($array)-1;$j++){
                    if($array[$j] < $array[$j+1]){
                        $temp = $array[$j+1];
                        $array[$j+1]=$array[$j];
                        $array[$j]=$temp;
                    }
                }
            }
            return response()->success($array);
        } catch (\Exception $e) {
            $msg['file'] = $e->getFile();
            $msg['message'] = $e->getMessage();
            $msg['line'] = $e->getLine();
            \Log::error($msg);
            return response()->error('Internal Server Error');
        } catch (\Illuminate\Database\QueryException $e) {
            $msg['file'] = $e->getFile();
            $msg['message'] = $e->getMessage();
            $msg['line'] = $e->getLine();
            \Log::error($msg);
            return response()->error('Database Error');
        }
    }
}
