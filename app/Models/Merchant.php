<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Transaction;
use App\Models\User;

class Merchant extends Model
{
    use HasFactory;
    protected $table='Merchants';
}
