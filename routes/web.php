<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\LoginController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::middleware(['authorization'])->group(function () {
    Route::get('/', [DashboardController::class,'dashboardv1'])->name('home');
    Route::get('/dashboard-v2', [DashboardController::class,'dashboardv2'])->name('dashboard-v2');
    Route::post('/dashboard-v2-filter', [DashboardController::class,'dashboardv2Filter']);
});

Route::get('/login',[LoginController::class,'index']);
Route::post('/login',[LoginController::class,'attempt'])->name('login');
Route::get('logout', [LoginController::class,'logout'])->name('login.logout');